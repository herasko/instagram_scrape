import scrapy
import json
import time
import os.path
import pandas as pd
import re
import os

from scrapy.exceptions import CloseSpider


class InstagramSpider(scrapy.Spider):
    handle_httpstatus_list = [429]
    name = "htag_cursor"

    def __init__(self):
        self.hashtag = os.getenv("HASHTAG")
        self.proxy = os.getenv("PROXY")
        if self.hashtag == '':
            self.hashtag = input("Name of the hashtag? ")
        self.start_urls = ["https://www.instagram.com/explore/tags/"+self.hashtag+"/?__a=1"]

    def parse(self, response):
        return self.parse_htag(response)

    def parse_htag(self, response):
        graphql = json.loads(response.text)
        has_next = graphql['graphql']['hashtag']['edge_hashtag_to_media']['page_info']['has_next_page']
        if has_next:
            end_cursor = graphql['graphql']['hashtag']['edge_hashtag_to_media']['page_info']['end_cursor']

            yield {'url':'https://www.instagram.com/explore/tags/' + self.hashtag + '/?__a=1&max_id=' + end_cursor}

            yield scrapy.Request("https://www.instagram.com/explore/tags/"+self.hashtag+"/?__a=1&max_id="+end_cursor,
                                 callback=self.parse_htag,
                                 meta={'proxy':self.proxy})