import scrapy
import json
import time
import os.path
import pandas as pd
import re

from scrapy.exceptions import CloseSpider


global count
count = 0

class InstagramSpider(scrapy.Spider):
    handle_httpstatus_list = [429]
    name = "hashtag"
    custom_settings = {
        'COOKIES_ENABLED':False
    }

    def __init__(self):
        self.hashtag = os.getenv("HASHTAG")
        self.proxy = os.getenv("PROXY")
        self.start_urls = ["https://www.instagram.com/explore/tags/"+self.hashtag+"/?__a=1"]

    def parse(self, response):
        return self.parse_htag(response)

    def parse_htag(self, response):
        a = 0
        if response.status == 429:
            time.sleep(6)
            yield scrapy.Request(response.url, callback=self.parse_htag, meta={'proxy':self.proxy}, dont_filter=True)
        else:
          global count
          graphql = json.loads(response.text)
          has_next = graphql['graphql']['hashtag']['edge_hashtag_to_media']['page_info']['has_next_page']
          edges = graphql['graphql']['hashtag']['edge_hashtag_to_media']['edges']
          count += len(edges)
          print(count)

          for edge in edges:
              a += 1
              node = edge['node']
              shortcode = node['shortcode']
              yield scrapy.Request("https://www.instagram.com/p/"+shortcode+"/?__a=1",
                                   callback=self.parse_post,
                                   meta={'proxy':self.proxy})

          if has_next and a == len(edges):
              end_cursor = graphql['graphql']['hashtag']['edge_hashtag_to_media']['page_info']['end_cursor']
              yield scrapy.Request("https://www.instagram.com/explore/tags/"+self.hashtag+"/?__a=1&max_id="+end_cursor,
                                   callback=self.parse_htag,
                                   meta={'proxy':self.proxy})
           
    def parse_post(self, response):
        if response.status == 429:
            time.sleep(6)
            yield scrapy.Request(response.url, callback=self.parse_post, meta=response.meta, dont_filter=True)
        else:
            graphql = json.loads(response.text)
            edges = graphql['graphql']['shortcode_media']['edge_media_to_tagged_user']['edges']
            post_url = "{0}{1}{2}".format('www.instagram.com/p/', graphql['graphql']['shortcode_media']['shortcode'], '/')
            for edge in edges:
              name = edge['node']['user']['username']
              url = '{0}{1}{2}'.format('www.instagram.com/', name, '/')
              yield scrapy.Request("https://" + url,
                                   callback=self.parse_item,
                                   meta={'proxy':self.proxy,
                                         'name':name,
                                         'url':url,
                                         'post_url':post_url})

    def parse_item(self, response):
        if response.status == 429:
            time.sleep(6)
            return scrapy.Request(response.url, callback=self.parse_item, meta=response.meta, dont_filter=True)
        else:
            text = response.selector.xpath('//script[@type="application/ld+json"]/text()').extract_first()
            email = ""
            phone = ""
            if text:
                text = json.loads(text)
                email = text.get('email')
                phone = text.get('telephone')
            return {'name':response.meta['name'],
                   'url':response.meta['url'],
                   'post_url':response.meta['post_url'],
                   'email':email,
                   'phone': phone}
